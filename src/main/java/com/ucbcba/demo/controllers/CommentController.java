package com.ucbcba.demo.controllers;

import com.ucbcba.demo.entities.Comment;
import com.ucbcba.demo.entities.Post;
import com.ucbcba.demo.entities.User;
import com.ucbcba.demo.services.CommentService;
import com.ucbcba.demo.services.PostService;
import com.ucbcba.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {

    private CommentService commentService;
    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    private PostService postService;

    @Autowired
    public void setPostService(PostService postService) { this.postService = postService; }


    private UserService userservice;
    @Autowired
    public void setUserservice(UserService userservice) {
        this.userservice = userservice;
    }



    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String save(Comment comment) {
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }

    @RequestMapping(value ="/commentlike/{id}/{idpost}",method = RequestMethod.GET)
    public String like(@PathVariable Integer id,@PathVariable Integer idpost,Model model) {
        Post post = postService.getPost(idpost);
        Comment comment = commentService.getComment(id);
        comment.setLikes(comment.getLikes()+1);
        commentService.saveComment(comment);
        return "redirect:/post/"+post.getId();
    }

    @RequestMapping(value ="/commentdislike/{id}/{idpost}",method = RequestMethod.GET)
    public String dislike(@PathVariable Integer id,@PathVariable Integer idpost,Model model) {
        Post post = postService.getPost(idpost);
        Comment comment = commentService.getComment(id);
        if (0<comment.getLikes()){
            comment.setLikes(comment.getLikes()-1);
            commentService.saveComment(comment);
        }
        return "redirect:/post/"+post.getId();
    }





}
