package com.ucbcba.demo.controllers;

import com.ucbcba.demo.entities.Post;
import com.ucbcba.demo.entities.PostCategory;
import com.ucbcba.demo.entities.User;
import com.ucbcba.demo.services.CommentService;
import com.ucbcba.demo.services.PostCategoryService;
import com.ucbcba.demo.services.PostService;
import com.ucbcba.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class PostController {
    private PostService postService;
    private PostCategoryService postCategoryService;
    private CommentService commentService;
    private UserService userservice;

    @Autowired
    public void setUserService(UserService userService) {
        this.userservice = userService;
    }

    @Autowired
    public void setPostService(PostService productService) {
        this.postService = productService;
    }

    @Autowired
    public void setPostCategoryService(PostCategoryService postCategoryService) {
        this.postCategoryService = postCategoryService;
    }

    /*@Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
    */

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String list(Model model) {
        Iterable<Post> postList = postService.listAllPosts();
        model.addAttribute("variableTexto","Hello world");
        model.addAttribute("postList",postList);
        return "posts";
    }

    @RequestMapping(value = "/newPost",method = RequestMethod.GET)
    public String newPost(Model model) {
        Iterable<PostCategory> postCategories = postCategoryService.listAllPostCategorys();
        Iterable<User> users = userservice.listAllUsers();

        model.addAttribute("postCategories", postCategories);
        model.addAttribute("users",users);
        return "newPost";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String save(@Valid Post post, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors())
        {
            return "newPost";
        }
        postService.savePost(post);
        return "redirect:/posts";
    }

    @RequestMapping("/post/{id}")
    public String show(@PathVariable Integer id, Model model) {
        Post post = postService.getPost(id);
        model.addAttribute("post", post);
        model.addAttribute("users",userservice.listAllUsers());
        return "show";
    }

    @RequestMapping("/editPost/{id}")
    public String editPost(@PathVariable Integer id, Model model) {
        Post post = postService.getPost(id);
        model.addAttribute("post", post);
        Iterable<PostCategory> postCategories = postCategoryService.listAllPostCategorys();
        model.addAttribute("postCategories", postCategories);
        model.addAttribute("users",userservice.listAllUsers());
        return "editPost";
    }

    @RequestMapping("/deletePost/{id}")
    public String delete(@PathVariable Integer id) {
        postService.deletePost(id);
        return "redirect:/posts";
    }

    @RequestMapping("/like/{id}")
    public String like(@PathVariable Integer id) {
        Post post = postService.getPost(id);
        post.setLikes(post.getLikes()+1);
        postService.savePost(post);
        return "redirect:/post/"+post.getId();
    }

    @RequestMapping("/dislike/{id}")
    public String dislike(@PathVariable Integer id) {
        Post post = postService.getPost(id);
        if (0<post.getLikes()){
            post.setLikes(post.getLikes()-1);
            postService.savePost(post);
        }
        return "redirect:/post/"+post.getId();
    }

    @RequestMapping("/")
    public String main(Model model) {
        Iterable<Post> postList = postService.listAllPosts();
        model.addAttribute("postList",postList);
        return "root";
    }


}
