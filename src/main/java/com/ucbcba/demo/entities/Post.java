package com.ucbcba.demo.entities;


import org.hibernate.annotations.ColumnDefault;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min = 1,message = "Tamaño Max de 100 letras",max = 100)
    private String text;

    @NotNull
    @Size(min = 1,message = "Tamaño Max de 20 letras",max = 20)
    private String title;

    @NotNull
    @Column(columnDefinition="int(11) default 0")
    private Integer likes=0;

    @ManyToOne
    @JoinColumn(name = "post_category_id")
    private PostCategory postCategory;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    List<Comment> comments;

    @NotNull
    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
    LocalDate creationDate;

    @NotNull
    @Size(min = 1,max = 2)
    private String showMostrar;

    public Post() {
    }


    public Integer getId() { return id; }
    public void setId(Integer id) { this.id = id; }


    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }


    public Integer getLikes() {
        return likes;
    }
    public void setLikes(Integer likes) {
        this.likes = likes;
    }


    public PostCategory getPostCategory() {
        return postCategory;
    }
    public void setPostCategory(PostCategory postCategory) {
        this.postCategory = postCategory;
    }


    public void setComments(List<Comment> comments){
        this.comments = comments;
    }
    public List<Comment> getComments(){ return this.comments; }


    public User getUser() { return user; }
    public void setUser(User user) { this.user = user; }


    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public LocalDate getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }


    public String getShowMostrar() {
        return showMostrar;
    }

    public void setShowMostrar(String showMostrar) {
        this.showMostrar = showMostrar;
    }
}
