delete from post_category;
delete from post;
delete from comment;
delete from user;


INSERT INTO post_category(id, name) VALUES (1000, 'Deportes');
INSERT INTO post_category(id, name) VALUES (1001, 'Noticias');
INSERT INTO post_category(id, name) VALUES (1002, 'Entretenimiento');

INSERT INTO post(id, text,title, post_category_id,user_id,creation_date,show_mostrar) VALUES (2000,'texto 1 del post', 'Post 1', 1000,1001,'2018-04-14','SI');
INSERT INTO post(id, text,title, post_category_id,user_id,creation_date,show_mostrar) VALUES (2002,'texto 2 del post', 'Post 2', 1001,1002,'2018-04-14','NO');

INSERT INTO comment(id,text,post_id,likes,date,user_id) VALUES (1001,'comentario_1',2000,1,'2018-04-14',1001);
INSERT INTO comment(id,text,post_id,likes,date,user_id) VALUES (1002,'comentario_2',2000,1,'2018-04-15',1001);
INSERT INTO comment(id,text,post_id,likes,date,user_id) VALUES (1003,'comentario_3',2002,1,'2018-04-16',1002);
INSERT INTO comment(id,text,post_id,likes,date,user_id) VALUES (1004,'comentario_4',2002,1,'2018-04-17',1003);

INSERT INTO user(id,name) VALUES (1001,'Usuario 1');
INSERT INTO user(id,name) VALUES (1002,'Usuario 2');
INSERT INTO user(id,name) VALUES (1003,'Usuario 3');

